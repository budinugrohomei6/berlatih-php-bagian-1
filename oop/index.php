<?php
require_once 'Animal.php';
require_once 'Frog.php';
require_once 'Ape.php';
$sheep = new Animal("shaun");

echo $sheep->getName();
echo $sheep->getLegs(4);
echo $sheep->getColdBlooded("no");
echo "<br>";

$Ape = new Ape("Kera Sakti");
echo $Ape->getName();
echo $Ape->getLegs(4);
echo $Ape->getColdBlooded("no");
echo $Ape->yell();
echo "<br>";
echo "<br>";

$frog = new Frog("buduk");
echo $frog->getName();
echo $frog->getLegs(4);
echo $frog->getColdBlooded("no");
echo $frog->jump();
