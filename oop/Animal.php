<?php
class Animal
{
    protected $name;
    protected $legs;
    protected $cold_blooded;


    public function __construct($name)
    {
        $this->name = $name;
    }
    public function getName()
    {
        return "Nama : " . $this->name . "<br>";
    }
    public function getLegs($legs)
    {
        return "Legs : " . $this->legs = $legs . "<br>";
    }
    public function getColdBlooded($cold_blooded)
    {
        return "Cold Blooded : " . $this->cold_blooded = $cold_blooded . "<br>";
    }
}
